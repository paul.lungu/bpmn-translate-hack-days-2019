'use strict';

module.exports = {
  name: 'Translate Labels To...',
  menu: './menu/menu.js',
  script: './client/client-bundle.js',
  style: './style/style.css'
};
