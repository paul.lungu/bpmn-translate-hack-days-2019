// import {
//   getPluginsDirectory
// } from 'camunda-modeler-plugin-helpers';

const T$ = require('./bpmn-translate-lib/Translatr.js');
var babelP = require('babel-polyfill');
var googleTranslate = require('google-translate')("AIzaSyC5Yrc1_EIMq5j7hofWUTUg5EcIicNveMI");

/**
 * A bpmn-js extension service, providing the actual
 * plug-in feature.
 */
function TranslatrPluginService(elementRegistry,
                                editorActions,
                                eventBus,
                                canvas,
                                modeling)
{
  this._elementRegistry = elementRegistry;
  this._modeling = modeling;

  var self = this;

  this.state = {
    open: false
  };

  //EXAMPLE: appending elements to the canvas
  // var img = document.createElement('img');
  // img.src = getPluginsDirectory() + '../../logo.png';
  // img.width = '50';

  // Object.assign(img.style, {
  //   position: 'absolute',
  //   bottom: '20px',
  //   left: '20px'
  // });

  // canvas.getContainer().appendChild(img);

  //EXMAPLE: reacting to an event on the canvas
  // eventBus.on('shape.added', function(event) {
  //   console.log('%c A shape was added to the diagram!', 'color: #52b415; font-size: 24px;');
  // });

   editorActions.register({
     translateToGerman: function() {
       //self.translate();
       console.log("...call code to translate current model to German...");
       // console.log("element registery"+ JSON.stringify(elementRegistry));
       var elements = getBPMNElements(elementRegistry);
       console.log("Elements: "+elements);
       var translationList = creatTranslationList(elements);
       console.log("Translation List: "+translationList);
       translate(translationList, elementRegistry, modeling);
     },
     translateToSpanish: function() {
       //self.translate();
       console.log("...call code to translate current model to Spanish...");
     },
     translateToEnglish: function() {
       //self.translate();
       console.log("...call code to translate current model to English...");
     }
   });

  /**
  * DENVER
  **/
  var getBPMNElements = function(elementRegistry) {
      //getting the elemnts from the bpmn dom
      // var elementRegistry = bpmnModeler.get('elementRegistry');
      // console.log(JSON.stringify(elementRegistry));
      var elements = new Array();
      elementRegistry.getAll().forEach(function (elem, gfx) {
        // Translating only the following flow node type,
        if (elem.businessObject.get('name') != undefined
            // && elem.businessObject.$instanceOf('bpmn:Event')
          ) {
            elements.push(elem);
        }
      });

      return elements;
  }

  /**
  ** DENVER
  ** Creates the list to pass to the translator
  *  Return: translationList
  **/
  var creatTranslationList = function (elements){

    return elements.map(function(elem, idx, arr){

      return elem.businessObject.get('name');

    });
  }

  /**
   *
   * DENVER
   *
   * This method will essentially refresh the diagram
   *
   ** example of properties
   ** var properties = {
   **  id: 'patquack' + Math.random(),
   **  name: "testing"  + Math.random()
   **  };
   **/
  var setBPMNProperties = function(element, properties, modeling) {
      //Getting modeling object to manipulate ????
      // var modeling = bpmnModeler.get('modeling');
      //set the properties and update the model
      // console.log(JSON.stringify(modeling));
      modeling.updateProperties(element, properties);
  }

  /*
  * Example using simple hardcoded translation
  */
  var translate = function(translationList, elementRegistry, modeling) {

    // gets a new object (the architecture allows us to not
    // have to use the 'new' keyword here)
    var translator = T$('Hola');
    // use our chainable methods
    return translator.setLang('es').translate(translationList,

      function(){
        //very simple implimentation of translations
        //normally you would do somthing with the taranslationList
        //like pass it to a service see google api example below
        var translations = ["Hola", "Tacos", "Serveca"];

        console.log("translations: "+translations);
        var mappedPropertiesList = mapTranslatedNames(translations, elementRegistry);
        console.log("Mapped properties: "+JSON.stringify(mappedPropertiesList));
        mappedPropertiesList.forEach(function (elem, idx) {
          var properties = createProperty(elem);
          console.log("Created Properites: "
              +JSON.stringify(properties));
              setBPMNProperties(elem.element, properties, modeling);
        });
      });
    }


    /**
    * EXAMPLE: getting the translations from GOOGLE API
    *
    var translate = function(translationList, elementRegistry, modeling) {

      // gets a new object (the architecture allows us to not
      // have to use the 'new' keyword here)
      var translator = T$('Hola');
      // use our chainable methods
      return translator.setLang('es').translate(translationList,

        function(){
          //google translation of API example
          googleTranslate.translate(translationList, 'en', 'de',
            function(err, translations){
              //use nested callback function to get results
              if (err) {
                throw new Error("Google API Malfunction!");
              }
              console.log("Google translations: "+translations);
              var mappedPropertiesList = mapTranslatedNames(translations, elementRegistry);
              console.log("Mapped properties: "+JSON.stringify(mappedPropertiesList));
              mappedPropertiesList.forEach(function (elem, idx) {
              var properties = createGoogleProperty(elem);
              console.log("Created Properites: "
                  +JSON.stringify(properties));
              setBPMNProperties(elem.element, properties, modeling);
            });
          });
        });
    }
    **/

  /**
  * DENVER
  * return a list of mapped translated properties
  **/
  var mapTranslatedNames = function(translatedItems){
    if(!Array.isArray(translatedItems)){
      translatedItems = new Array(translatedItems)
    }

    return translatedItems.map(function (elem, idx, arr) {
        var elements = getBPMNElements(elementRegistry);

        return {
            id: elements[idx].businessObject.get('id'),
            name: translatedItems[idx],
            element: elements[idx]
        };
    });
  }

  /**
  * DENVER
  * Create the properties to update the model
  **/
  var createProperty = function(translatedElement){
      return {
        id: translatedElement.id,
        name: translatedElement.name
      }
  }

  /**
  * DENVER
  * Create the properties from google API elements to update the model
  **/
  var createGoogleProperty = function(translatedElement){
      return {
        id: translatedElement.id,
        name: translatedElement.name.translatedText
      }
  }
}

TranslatrPluginService.$inject = [ 'elementRegistry', 'editorActions', 'eventBus', 'canvas', 'modeling' ];


/**
 * The service and it's dependencies, exposed as a bpmn-js module.
 *
 * --------
 *
 * WARNING: This is an example plug-in.
 *
 * Make sure you rename the plugin and the name it is exposed (PLEASE_CHANGE_ME)
 * to something unique.
 *
 * --------
 *
 */
// export default {
//   __init__: [ 'TRANSLATR_PLUGIN_SERVICE' ],
//   TRANSLATR_PLUGIN_SERVICE: [ 'type', TranslatrPluginService ]
// };

module.exports = {
    __init__: ['TRANSLATR_PLUGIN_SERVICE'],
    TRANSLATR_PLUGIN_SERVICE: ['type', TranslatrPluginService]
};
