import $ from 'jquery';
import BpmnModeler from 'bpmn-js/lib/Modeler';
const T$ = require('./bpmn-translate-lib/Translatr.js');
const I18n$ = require('./bpmn-i18n-lib/i18n-parser.js');
import translation from '../resources/translation.json';

import propertiesPanelModule from 'bpmn-js-properties-panel';
import propertiesProviderModule from 'bpmn-js-properties-panel/lib/provider/camunda';
import camundaModdleDescriptor from 'camunda-bpmn-moddle/resources/camunda.json';
//DENVER:  Adding library that allows for async operations in node js code
import 'babel-polyfill';
//
import {debounce} from 'min-dash';
import diagramXML from '../resources/newDiagram.bpmn';

//Api null for now, need to setup env variable
var api = "null for now";
// DENVER
var googleTranslate = require('google-translate')(api);
//
var container = $('#js-drop-zone');
var canvas = $('#js-canvas');

var bpmnModeler = new BpmnModeler({
    container: canvas,
    propertiesPanel: {
        parent: '#js-properties-panel'
    },
    additionalModules: [
        propertiesPanelModule,
        propertiesProviderModule
    ],
    moddleExtensions: {
        tr: translation,
        camunda: camundaModdleDescriptor
    }
});
container.removeClass('with-diagram');

function createNewDiagram() {
  openDiagram(diagramXML);
}

function openDiagram(xml) {
    bpmnModeler.importXML(xml, function (err) {
        if (err) {
            container
                .removeClass('with-diagram')
                .addClass('with-error');
            container.find('.error pre').text(err.message);
        } else {
            container
                .removeClass('with-error')
                .addClass('with-diagram');
        }
    });
}

/**

 **/
function saveDiagram(done) {
    bpmnModeler.saveXML({format: true}, function (err, xml) {
        done(err, xml);
    });
}

/**
 DENVER
 **/
function getBPMNElements() {
    //getting the elemnts from the bpmn dom
    var elementRegistry = bpmnModeler.get('elementRegistry');

    var elements = new Array();
    elementRegistry.forEach(function (elem, gfx) {
      // Translating only the following flow node type,
      if (elem.businessObject.get('name') != undefined
          // && elem.businessObject.$instanceOf('bpmn:Event')
        ) {
          elements.push(elem);
      }
    });

    return elements;
}

/**
** DENVER
** Creates the list to pass to the translator
*  Return: translationList
**/
function creatTranslationList(elements){

  return elements.map(function(elem, idx, arr){

    return elem.businessObject.get('name');

  });
}

/**
 *
 * DENVER
 *
 * This method will essentially refresh the diagram
 *
 ** example of properties
 ** var properties = {
 **  id: 'patquack' + Math.random(),
 **  name: "testing"  + Math.random()
 **  };
 **/
function setBPMNProperties(element, properties) {
    //Getting modeling object to manipulate ????
    var modeling = bpmnModeler.get('modeling');
    //set the properties and update the model
    modeling.updateProperties(element, properties);
}

/*
DENVER
 */
function translate(translationList) {

  // gets a new object (the architecture allows us to not
  // have to use the 'new' keyword here)
  var translator = T$('Hola');

  // use our chainable methods
  return translator.setLang('es').translate(translationList,
      function(gtranslations){
        var mappedPropertiesList = mapTranslatedNames(gtranslations);
        mappedPropertiesList.forEach(function (elem, idx) {
        var properties = createProperty(elem);
        setBPMNProperties(elem.element, properties);
      });

    });
}

/**
* DENVER
* return a list of mapped translated properties
**/
function mapTranslatedNames(translatedItems){
  if(!Array.isArray(translatedItems)){
    translatedItems = new Array(translatedItems)
  }

  return translatedItems.map(function (elem, idx, arr) {
      var elements = getBPMNElements();

      return {
          id: elements[idx].businessObject.get('id'),
          name: translatedItems[idx],
          element: elements[idx]
      };
  });
}

/**
* DENVER
* Create the properties to update the model
**/
function createProperty(translatedElement){
    return {
      id: translatedElement.id,
      name: translatedElement.name.translatedText
    }
}

/**
*
**/
function registerFileDrop(container, callback) {

    function handleFileSelect(e) {
        e.stopPropagation();
        e.preventDefault();
        var files = e.dataTransfer.files;
        var file = files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            var xml = e.target.result;
            callback(xml);
        };
        reader.readAsText(file);
    }

    function handleDragOver(e) {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    container.get(0).addEventListener('dragover', handleDragOver, false);
    container.get(0).addEventListener('drop', handleFileSelect, false);
}

////// file drag / drop ///////////////////////

// check file api availability
if (!window.FileList || !window.FileReader) {
    window.alert(
        'Looks like you use an older browser that does not support drag and drop. ' +
        'Try using Chrome, Firefox or the Internet Explorer > 10.');
} else {
    registerFileDrop(container, openDiagram);
}

// bootstrap diagram functions

$(function () {

    $('#js-create-diagram').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        createNewDiagram();
    });

    //Start JS Translate function
    $('#js-translate').click(function (e) {

        // alert("translate stuff");
        var elements = getBPMNElements();
        var translationList = creatTranslationList(elements);
        translate(translationList);

    });//end JS translate function

    var downloadLink = $('#js-download-diagram');
    $('.buttons a').click(function (e) {
        if (!$(this).is('.active')) {
            alert('hello');
            e.preventDefault();
            e.stopPropagation();
        }
    });

    function setEncoded(link, name, data) {
        var encodedData = encodeURIComponent(data);
        if (data) {
            link.addClass('active').attr({
                'href': 'data:application/bpmn20-xml;charset=UTF-8,' + encodedData,
                'download': name
            });
        } else {
            link.removeClass('active');
        }
    }

    //Used to highlight Translate button
    function setEncoded2(link) {
        if (link) {
            link.addClass('active').attr({
                'href': '#'
            });
        } else {
            link.removeClass('active');
        }
    }

    var exportArtifacts = debounce(function () {
        saveDiagram(function (err, xml) {
            setEncoded(downloadLink, 'diagram.bpmn', err ? null : xml);
            setEncoded2($('#js-translate'));
        });
    }, 500);

    bpmnModeler.on('element.changed', function (event) {
        var element = event.element;
        // the element was changed by the user
    });

    bpmnModeler.on('commandStack.changed', exportArtifacts);
});
