'use strict';

import translation from '../resources/translation.json';
import BpmnModdle from 'bpmn-moddle';

// 'new' an object
var Parser = function(name, language) {
    return new Parser.init(name, language);
}

// the actual object is created here, allowing us to 'new' an object
// without calling 'new'
Parser.init = function(name, language) {

    var self = this;
    self.name = name || '';
    self.language = language || 'en';

}

// trick borrowed from jQuery so we don't have to use the 'new' keyword
Parser.init.prototype = Parser.prototype;


(function(global, proto) {

    // hidden within the scope of the IIFE and never directly accessible
    var supportedLangs = ['en', 'es'];

    // translations
    var translations = {
        en: 'Hello',
        es: 'Hola'
    };

    // logger messages
    var logMessages = {
        en: 'Logged in',
        es: 'Inició sesión'
    };

    // proto holds methods (to save memory space)
    proto.validate = function() {
        // check that is a valid language
        // references the externally inaccessible 'supportedLangs' within the closure
         if (supportedLangs.indexOf(this.language)  === -1) {
            throw "Invalid language";
         }
         console.log("\n\n [#] Language validated");

         return this;
    }

    // 'this' refers to the calling object at execution time
    proto.label = function() {
        return this.name;
    },

    // retrieve messages from object by referring to properties using [] syntax
    proto.translation = function() {
        return translations[this.language] + ' ' + this.name + '!';
    },

    // chainable methods return their own containing object
    proto.parse = function() {
        var msg;

        // if undefined or null it will be coerced to 'false'
        msg = this.translation();

        if (console) {
            console.log("\n\n [#] "+msg);
        }

        // 'this' refers to the calling object at execution time
        // makes the method chainable
        return this;
    },

    proto.log = function() {
        if (console) {
            console.log("\n\n [#] "+logMessages[this.language] + ': ' + this.label());
        }

        // make chainable
        return this;
    },

    proto.setLang = function(lang) {

        // set the language
        this.language = lang;

        // validate
        this.validate();

        // make chainable
        return this;
    }

    // attach our Parser to the global object, and provide a shorthand '$G' for ease our poor fingers
    // global.Parser = global.T$ = Parser;

}(global, Parser.prototype));

module.exports = Parser;

function addExtensionElements(bpmnModeler, elem, before, after) {

    var moddle = bpmnModeler.get('moddle');

    var modeling = bpmnModeler.get('modeling');

    var extensionElements = elem.businessObject.extensionElements || moddle.create('bpmn:ExtensionElements');

    var translateDetails = getExtensionElement(elem.businessObject, 'tr:TranslationDetails');

    if (!translateDetails) {
        translateDetails = moddle.create('tr:TranslationDetails');

        extensionElements.get('values').push(translateDetails);
    }

    translateDetails.before = before;
    translateDetails.after = after;

    modeling.updateProperties(elem, {
        extensionElements
    });

    return modeling;

}

function getExtensionElement(element, type) {
    if (!element.extensionElements) {
        return;
    }

    return element.extensionElements.values.filter((extensionElement) => {
        return extensionElement.$instanceOf(type);
})[0];
}