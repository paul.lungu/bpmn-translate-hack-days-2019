'use strict';
//Adding library that allows for async operations in node js code
import 'babel-polyfill';
var async = require("async");
//Api null for now, need to setup env variable
var api = "AIzaSyC5Yrc1_EIMq5j7hofWUTUg5EcIicNveMI";
var googleTranslate = require('google-translate')(api);
// 'new' an object

var Translatr = function(name, language) {
    return new Translatr.init(name, language);
}

// the actual object is created here, allowing us to 'new' an object
// without calling 'new'
Translatr.init = function(name, language) {

    var self = this;
    self.name = name || '';
    self.language = language || 'en';

}

// trick borrowed from jQuery so we don't have to use the 'new' keyword
Translatr.init.prototype = Translatr.prototype;


(function(global, proto) {

    // hidden within the scope of the IIFE and never directly accessible
    var supportedLangs = ['en', 'es'];

    var translations;

    // proto holds methods (to save memory space)
    proto.validate = function() {
        // check that is a valid language
        // references the externally inaccessible 'supportedLangs' within the closure
         if (supportedLangs.indexOf(this.language)  === -1) {
            throw "Invalid language";
         }

         return this;
    },

    // retrieve messages from object by referring to properties using [] syntax
    /**
    0: {translatedText: "Hallo", originalText: "Hello"}
    1: {translatedText: "Vielen Dank", originalText: "Thank you"}
    **/
    proto.translation = function(translationList, fun) {
      //getting the translated payload form the API
      googleTranslate.translate(translationList, 'en', 'de',
        function(err, translations){
          //use nested callback function to get results
          fun(translations);
        });
    },

    // chainable methods return their own containing object
    proto.translate = function(translationList, fun) {
        // if undefined or null it will be coerced to 'false'
        this.translation(translationList, fun);

        // makes the method chainable
        return this;
    },

    proto.setLang = function(lang) {

        // set the language
        this.language = lang;

        // validate
        this.validate();

        // make chainable
        return this;
    }

    // attach our Translatr to the global object, and provide a shorthand '$G' for ease our poor fingers
    // global.Translatr = global.T$ = Translatr;

}(global, Translatr.prototype));

module.exports = Translatr;
