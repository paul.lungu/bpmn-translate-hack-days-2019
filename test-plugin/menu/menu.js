'use strict';

module.exports = function(electronApp, menuState) {
  return [

    {
      label: 'English',
      accelerator: 'CommandOrControl+.',
      enabled: function() {
        return true;
      },
      action: function() {
        electronApp.emit('menu:action', 'translateToEnglish');
      }
    }, {
      label: 'Spanish',
      accelerator: 'CommandOrControl+-',
      enabled: function() {
        return true;
      },
      action: function() {
        electronApp.emit('menu:action', 'translateToSpanish');
      },

    },{

      label: 'German',
      accelerator: 'CommandOrControl+g',
      enabled: function() {
        return true;
      },
      action: function() {
        electronApp.emit('menu:action', 'translateToGerman');
      }


    }

  ];
};