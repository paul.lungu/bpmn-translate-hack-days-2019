import {
  getPluginsDirectory
} from 'camunda-modeler-plugin-helpers';

/**
 * A bpmn-js extension service, providing the actual
 * plug-in feature.
 */

function TranslatorPluginService(elementRegistry, editorActions, eventBus, canvas, modeling) {
  this._elementRegistry = elementRegistry;
  this._modeling = modeling;

  var self = this;

  this.state = {
    open: false
  };

  editorActions.register({
    translateToGerman: function() {
      //self.translate();
      console.log("...call code to translate current model to German...");
    },
    translateToSpanish: function() {
      //self.translate();
      console.log("...call code to translate current model to Spanish...");
    },
    translateToEnglish: function() {
      //self.translate();
      console.log("...call code to translate current model to English...");
    }
  });

  // this.addRenameIDsContainer(canvas.getContainer().parentNode);
}

TranslatorPluginService.$inject = [ 'elementRegistry', 'editorActions', 'eventBus', 'canvas', 'modeling' ];

/**
 * The service and it's dependencies, exposed as a bpmn-js module.
 *
 * --------
 *
 * WARNING: This is an example plug-in.
 *
 * Make sure you rename the plugin and the name it is exposed (PLEASE_CHANGE_ME)
 * to something unique.
 *
 * --------
 *
 */
export default {
  __init__: [ 'PLEASE_CHANGE_ME' ],
  PLEASE_CHANGE_ME: [ 'type', TranslatorPluginService ]
};
